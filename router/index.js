import express from "express";
import alumnosDB from "../models/alumnos.js";

export const router = express.Router();
export default { router };

router.get("/", (req, res) => {
	res.render("index", { titulo: "Mis prácticas Node.js", nombre: "Luis Antonio Romero Villanueva" });
});

router.get("/tabla", (req, res) => {
	const params = {
		numero: req.query.numero,
	};
	res.render("tabla", params);
});

router.post("/tabla", (req, res) => {
	const params = {
		numero: req.body.numero,
	};
	res.render("tabla", params);
});

router.get("/cotizacion", (req, res) => {
	const params = {
		valor: req.query.valor,
		pinicial: req.query.pinicial,
		plazos: req.query.plazos
	};
	res.render("cotizacion", params);
});

router.post("/cotizacion", (req, res) => {
	const params = {
		valor: req.body.valor,
		pinicial: req.body.pinicial,
		plazos: req.body.plazos
	};
	res.render("cotizacion", params);
});

router.get("/alumnos", async(req, res) => {
	const resultado = await alumnosDB.mostrar();
	res.render("alumnos", {reg:resultado});
});

router.post("/alumnos", async(req, res) => {
	let params;
	try{
		params = {
			matricula:req.body.matricula,
			nombre:req.body.nombre,
			domicilio:req.body.domicilio,
			sexo:req.body.sexo,
			especialidad:req.body.especialidad
		}
		const res = await alumnosDB.insertar(params);
	} catch (error){
		console.error(error);
		res.status(400).send("Sucedió un error: ",error);
	}
	res.redirect("/alumnos");
});