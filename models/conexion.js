import mysql from "mysql2";

var conexion = mysql.createConnection({
    host:"127.0.0.1",
    user:"root",
    password:"micontraseña",
    database:"sistemas"
});
conexion.connect(function(err){
    if(err){
        console.log("Surgio un error al conectar: ",err);
    }
    else{
        console.log("Se conecto con exito");
    }
})

export default conexion