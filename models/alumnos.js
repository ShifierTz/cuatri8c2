import conexion from "./conexion.js";

var alumnosDB = {}
alumnosDB.insertar = function insertar(alumno){
    return new Promise((resolve,rejects) =>{
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                console.log("Surgió un error",err.message);
                rejects(err);
            } else{
                const alumno = {
                    id:res.id
                }
                resolve(alumno);
            }
        })
    })
}

alumnosDB.mostrar = function mostrar(){
    return new Promise ((resolve,rejects) => {
        let sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta,null,function(err,res){
            if(err){
                console.log("Surgió un error", err);
                rejects(err);
            } else{
                resolve(res);
            }
        })
    })
}

export default alumnosDB;
